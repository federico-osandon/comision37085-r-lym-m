import { useState, useEffect } from "react";


 
export const ControlledInput = () => {
    const [input, setInput] = useState( {texto: ''} );

    useEffect(() => {
      console.log('subcripcion');
    }, [input])

    const handleOnChange = (e) =>{
      console.log(e.target.name)
      console.log(e.target.value)
      
    }
    
    return (
      <input
        type="text"
        name='texto'
        value={input.texto}
        onChange={handleOnChange}
      />
    );
  };








  
export  function LoadingComponent() {
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {

      setTimeout(() => {
        setLoading(false);
      }, 5000);

      return ()=>{
          console.log('Limpiando componente');
      }
    }, []);
    
    return <>
        {loading ? <h2>Loading... </h2> : <h3>Productos cargardos!</h3>}
    </>;
  }











  
export  function TextComponent({ existeUsuario = true, children }) { // if con return  temprano
    
    if (!existeUsuario) {
      return <h2>Uds no esta logeado no puede ver nada</h2>;
    }
  
    return (
      <>  
        {/* <h2>Ud esta logueado puede ver la pág.</h2> */}
        {children}
      </>
    )
  }








// condicion ? :,  condicion && , condicion || 


export  function TextComponent2({ condition = true }) { // niveles de jerarquía en usuarios
    
    return (
      <>
        <button>Boton 1</button>
        {condition && <button>Button Para NO administrador</button>}

        {!condition && <button>Buton para  Admin</button>}

      </>
    );
  }

// condicion ? :(si no), condición && accion si, condicion  || acciones







export  function TextComponent3({ condition = true }) {
    return (
      <>
        <h2> {condition ? 'Ud esta logueado puede ver la pág.' : 'Ud esta logueado no puede ver la pág.'} </h2>            
        
      </>
    )
  }











// condicion ?: (si no), condición && accion si, condicion  || acciones

 export function TextComponent4({ condition = true }) {

    return (
      <>
        <h2 style={ { color: condition ? "green" : "red" } }>
          stock
        </h2>
      </>
    );
  }













  
export  function TextComponent5({ condition = true }) {
    return (
      <>
        <h2 className={ (condition === true) ? "btn btn-success" : "btn btn-danger" }>
         stock
        </h2>
      </>
    );
  }

















export  function TextComponent6( { condition = true, otro='mt-5' }  ) {
    return (
      <>
        <h2
          className={ `${condition === true ? "btn btn-success" : "btn btn-danger"} ${otro || ""} `}
        >
          Ud esta logueado puede ver la pág.
        </h2>
      </>
    );
  }












export function TextComponent7({ condition = false , otro = "mt-5" }) {
    
    const config = condition

      ?
            {
                className: `btn btn-success ${otro || ""}`,
                style: {color: 'red'},
                title: "Este es el titulo si la condicion es verdadera",
                nombre: 'Fede'
            }
      : 
            {
                // className: `btn btn-warning ${otro || ""}`,
                // style: {color: 'green'},
            }

            
    
      return (
      <>
        <h2 {...config} >Ud esta logueado puede ver la pág.</h2>
      </>
    )
  }