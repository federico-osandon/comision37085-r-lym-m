import { getFirestore, doc, getDoc } from "firebase/firestore"
import { useState } from "react"
import { useEffect } from "react"
import { useParams } from "react-router-dom"

import { TextComponent2, TextComponent3, TextComponent4, TextComponent5, TextComponent6, TextComponent7 } from "../../ComponenteEjemplo/ComponenteEjemplosCondicionales"
import ItemDetail from "../../ItemDetail/ItemDetail"

const ItemDetailContainer = () => {
    // estados 
    const [ product, setProduct ] = useState({})
    const [ loading, setLoading ] = useState(true)
    const { idProducto } = useParams()
    //traer un producto de firebase
    useEffect(() => {
        const db = getFirestore()
        const queryDoc = doc(db, 'items', idProducto)
        getDoc(queryDoc)
        .then(resp => setProduct( {id: resp.id, ...resp.data()} ))
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
    },[])
    console.log(product)
    return (
            
            loading ? 
                <h1>Cargando...</h1> 
            : 
                <ItemDetail product={product} />    
            // <TextComponent6 otro='mt-2' />
            // <TextComponent7 />
        
        
    
    )
}

export default ItemDetailContainer