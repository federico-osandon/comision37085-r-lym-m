import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"

import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore'

import ItemList from "../../ItemList/ItemList"
import Titulo from "../../Titulo/Titulo"



const Loading = () => {    
    return <h2>Cargando...</h2>
} 
 
const ItemListContainer = ( {greeting, titulo} ) => {
    const [ productos, setProductos ] = useState([])   
    const [ loading, setLoading ] = useState(true)
    const { idCategoria } = useParams()
    
    /**
     * If there's an idCategoria, then query the collection with the idCategoria, otherwise query the
     * collection without the idCategoria.
     */
        const traerProductos = () => {
            const db = getFirestore()
            const queryCollection = collection(db, 'items')
            const queryFiltrada = idCategoria ? query(queryCollection, where('categoria', '==', idCategoria))  
                : queryCollection               
            getDocs(queryFiltrada)
            .then(resp => setProductos(resp.docs.map(prod => ({ id: prod.id, ...prod.data() }) ))) 
            .catch(err => console.log(err))
            .finally(() => setLoading(false))
        }

    useEffect(() => {
        traerProductos()
    },[idCategoria])    

    return (
        <div className="border border-5 border-success">
           <Titulo titlo={'titulo de itemListContainer'} subtitulo={'Subtitulo de itemlistcontainer'} />
            <p>{greeting}</p>             

            { loading ? 
                    <Loading />
                :
                    <>
                        <ItemList productos={productos} /> 
                    </>
            }
        </div>
    )
}

export default ItemListContainer










/// no repetir código
// no mezclar idiomas en el código
// si no lo neceistamo que no esté -> código muerto -> eliminarlo y funciones que no se usan
// no dejar console.log
// Declaracioes de variables o importaciones sin usar -> eliminarlas
// modularizar el código









// const obj = {
//     name: 'Juan'
// }






