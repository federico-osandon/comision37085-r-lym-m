import {BrowserRouter, Routes, Route, Navigate} from 'react-router-dom'
// const NavBar = require('./components/NavBar')
import ItemListContainer from './components/Pages/ItemListContainer/itemListContainer'
import NavBar from './components/NavBar/NavBar'
import ItemDetailContainer from './components/Pages/ItemDetailContainer/ItemDetailContainer'
import CarritoPage from './components/Pages/CarritoPage/CarritoPage'
import { NotFound404 } from './components/NotFound404/NotFound404'

// custom hook
// imagenes del icono que se rompe
// no repetir item, cantidad borrar por item del carrito 


import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import CartContextProvider from './context/cartContext'
import { TextComponent } from './components/ComponenteEjemplo/ComponenteEjemplosCondicionales'

function App() {
    
    return (
        <div 
        // className='border border-5 border-primary' 
        // onClick={() => alert('Soy el click de app')}
        >
            <CartContextProvider>
                <BrowserRouter>

                    <NavBar />
                    
                    <Routes>     
                        <Route path='/'  element={<ItemListContainer greeting={'Hello World'} titulo='ItemListContainer de app' />} />  
                        
                        <Route path='/categoria/:idCategoria' element={<ItemListContainer greeting={'Hello World'} titulo='ItemListContainer de app' />} />    
                        {/* <Route path='/categoria/gorras' element={<ItemListContainer greeting={'Hello World'} titulo='ItemListContainer de app' />} />    
                        <Route path='/categoria/pantalones' element={<ItemListContainer greeting={'Hello World'} titulo='ItemListContainer de app' />} />     */}

                        <Route path='/detalle/:idProducto'  element={<TextComponent><ItemDetailContainer /></TextComponent>} />      
                        <Route path='/cart'  element={<CarritoPage />} /> 
                        <Route path='/404'  element={<NotFound404 />} />                    
                                        
                        
                        <Route path='*' element={ <Navigate to='/404' />} />
                    </Routes>            
                </BrowserRouter>
            </CartContextProvider>
        </div>
    )
}

export default App
