
import {  useState, useEffect } from 'react'
 
const ItemListContainer = ( {greeting, titulo} ) => {

    const [cont, setCont] = useState(0)
    const [fecha, setFecha] = useState(Date())
    // state booleano
    const [bool, setBool] = useState(true)
    
    useEffect(()=>{ // ejecuta siempre 
        // acciones que querramos que se
        console.log('addEventListner')
        // addeventListener(evento, callback)

        return ()=>{
            /// removeeventListener
            console.log('removeeventListener')
        }
    })

    // useEffect(()=>{ // ejecuta siempre 
    //     // acciones que querramos que se
    //     console.log('llamada a api - 2')
    // }, [])

    // useEffect(()=>{ // ejecuta siempre 
    //     // acciones que querramos que se
    //     console.log('ejecución cuando cambio solo bool - 3')
    // }, [bool])

    const handleClick = () => {
        // contador++ // contador = contador + 1
        setCont(cont + 1)
        setFecha(Date())
    }
    
    
    console.log('ItemListContainer -  4')
    return (
        <div>
            <div>{ titulo }</div>
            <div>{ cont }</div>
            <div>{ fecha }</div>
            <button onClick={ handleClick } >click</button>
            <button onClick={ ()=> setBool(!bool)} >click</button>
            {/* <button onClick={ handleClick } >click</button> */}
            {/* <Component /> */}
        </div>
    )
}

export default ItemListContainer