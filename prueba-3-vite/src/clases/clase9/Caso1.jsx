import { useState } from "react";
///////////////////////////////////// componente Select ////////////////////////////////////

function Select({ options, onSelect, defaultOption=1 }) {

  return (

    <select onChange={(evt) => onSelect(Number(evt.target.value))}>

      {
        options.map((o) => (
            <option value={o.value}>{o.text}</option>
        ))
      }

    </select>

  );
}


///////////////////////////////////////////////////////////////////////////

const options = [
    { value: 1, text: "Azul" },
    { value: 2, text: "Rojo" }
  ];


  // componente pricinpal
export default function Caso1() {
  
  const [option, setOption] = useState(1);

    function optionSelected(value) {
        setOption(value);
        //console.log(value)
    }
    
    console.log(option)
    return (
        <>
            {option===1 ? 
                    <img className="w-25" src='https://static.dafiti.com.ar/p/tommy-hilfiger-6711-936993-1-product.jpg' alt='foto' />
                    : 
                    <img className="w-25" src='https://static.dafiti.com.ar/p/everlast-0575-351435-1-product.jpg' alt='foto' /> 
            }


            <Select 
                options={options} 
                onSelect={optionSelected} 
                defaultOption={option} 
            />        

            
          
        </>
    );
}
