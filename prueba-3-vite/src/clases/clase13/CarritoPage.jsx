
import { addDoc, collection, doc, documentId, getDocs, getFirestore, query, updateDoc, where, writeBatch } from 'firebase/firestore'
import { useState } from 'react'
import { useCartContext } from '../../../context/cartContext'
// import  useContext from 'react'
// import CartContext from '../context/cartContext'


export default function CarritoPage(){

    const  [dataForm, setDataForm] = useState({
        name: '',
        phone: '',
        email:''

    })
    const { cartlist, vaciarCarrito, precioTotal } = useCartContext()


    const generarOrden = async (e)=>{
        e.preventDefault()
        const orden = {}
        
        orden.buyer= {
            name: dataForm.name,
            phone: dataForm.phone,
            email: dataForm.email
        }

        orden.items= cartlist.map(prod => {
            const {id, name: title, price} = prod
            return {id, title, price}
        })

        orden.total= precioTotal()

        const db = getFirestore()
        const orders = collection(db, 'orders')
        addDoc(orders, orden) // setDoc(orders, obj, id)
        .then(resp => console.log(resp))
        .catch(err => console.log(err))
        .finally(() => vaciarCarrito())

        // update

        // const orderDoc = doc(db, 'items', 'NOckCyZmwCiQajP2XydB')    
        // updateDoc(orderDoc, {
        //     stock: 100
        // })
        // .then(resp => console.log('Producto actualizado'))
        // .catch(err => console.log(err))    
        // console.log(orden)

         // OPCIONAL

        // const queryCollection = collection(db, 'items')

        // const queryActulizarStock = await query(
        //   queryCollection, //                   ['jlksjfdgl','asljdfks'],  
        //   where( documentId() , 'in', cartlist.map(it => it.id) )          
        // )

        // const batch = writeBatch(db)

        // await getDocs(queryActulizarStock)
        //   .then(resp => resp.docs.forEach(res => batch.update(res.ref, {
        //       stock: res.data().stock - cartlist.find(item => item.id === res.id).cantidad
        // }) ))

        // batch.commit()
        // console.log('sotck actualizado')
    }
//   console.log(cartlist)

    const handleInputChange = (e) => {
        // console.log(e.target.name)
        // console.log(e.target.value)
        setDataForm({
            ...dataForm,
            [e.target.name]: e.target.value
        })
    }
    console.log(dataForm)
    return (
            <div>

                <h1>Carrito</h1>
            <ul>
                {cartlist.map(producto => <li> nombre: {producto.name} categoría:{producto.categoria} precio: {producto.price} Cant: {producto.cantidad} </li> )}
            </ul>
            <h2>Total: {precioTotal()}</h2>

            {/* fomulario para la orden */}
            <form onSubmit={generarOrden}>
                <input 
                    type="text" 
                    name="name"
                    placeholder="Nombre" 
                    value={dataForm.name}
                    onChange={handleInputChange}
                />
                <input 
                    type="text"
                    name="phone" 
                    value={dataForm.phone}
                    placeholder="Teléfono" 
                    onChange={handleInputChange}
                />
                <input 
                    type="text" 
                    name="email"
                    value={dataForm.email}
                    placeholder="Email" 
                    onChange={handleInputChange}
                />
                <button type="submit">Generar orden</button>
            </form>
            <button onClick={vaciarCarrito}>Vaciar carrito</button>
            {/* <button onClick={generarOrden}>Generar orden</button> */}
        </div>
  )
}

