// console.log('Hola archivo index.js');

// var nombre = 'Juan'

// let apellido = 'Perez'


// objPersona.nombre = 'Juan Carlos'


{
    const edad = 30

    console.log( edad)
}

// console.log( objPersona.nombre)


let condition = true
let  mostrar = ''

// if (condition) {
//     mostrar = 'verdadero'
// } else {
//     mostrar = 'falso'
// }

// console.log('ESto es : ' + mostrar + 'porque  la condicion es ' + condition)
console.log(`Esto es : ${ condition ? 'verdadero' : 'falso' } porque la condicion es ${condition}`)
// condition && 'verdaderp' -> condition || 'falso'


// destructuracion de objetos

const objPersona = {
    nombre: 'Juan',
    apellido: 'Perez'
}


// const nombre = objPersona.nombre
// const apellido = objPersona.apellido

const { nombre, apellido } = objPersona

// console.log(nombre)
// console.log(apellido)

let array = [2,3,4,5,6,7,8,9,10]
let uno = 1
const newArray = [  ...array, uno ]
// console.log(newArray)

const saludar1 = (nombre = 'juan')=>{
    console.log(nombre )
}

saludar1('Fede')